﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SwoleFile.Entities
{
    public class ExercisesToWorkouts
    {

        public int Id { get; set; }

        public string ExerciseId { get; set; }

        public string WorkoutId { get; set; }

        public string OrderNumber { get; set; }

    }
}
