﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SwoleFile.Entities
{
    public class WorkoutsToPlans
    {

        public int Id { get; set; }

        public string WorkoutId { get; set; }

        public string PlanId { get; set; }

        public string StageNumber { get; set; }

    }
}
