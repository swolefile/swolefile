﻿var ViewModel = function () {
    var self = this;
    self.users = ko.observableArray();
    self.error = ko.observable();

    var userUri = '/api/GetAll/';

    function ajaxHelper(uri, method, data) {
        self.error(''); // Clear error message
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

    function getAllUsers() {
        ajaxHelper(userUri, 'GET').done(function (data) {
            self.users(data);
        });
    }

    // Fetch the initial data.
    getAllUsers();
};

ko.applyBindings(new ViewModel());