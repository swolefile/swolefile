﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using SwoleFile.Models;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNet.Authentication;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Identity;
using SwoleFile.Entities;

namespace SwoleFile.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public IActionResult Index()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewBag.Message = "Your Ass page.";

            return View();
        }


        [Authorize]
        public IActionResult Profile()
        {
            return View();
        }

        public IActionResult Plans()
        {
            return View();
        }

        public IActionResult ViewPlan()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
        }

        public IActionResult Checkin()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            using (var ctx = new ApplicationDbContext())
            {
                CheckIn ck = new CheckIn() { TimeStamp = DateTime.Now, UserId = User.GetUserId() };
                ctx.CheckIns.Add(ck);
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public IActionResult Test()
        {
            //var sid = User.GetUserId();
            LoginUserModel model = new LoginUserModel();
            IdentityUserLogin<string> userlogin = new IdentityUserLogin<string>();

            using (var dbContext = new ApplicationDbContext())
            {
                userlogin = dbContext.UserLogins.First();
                model.ProviderKey = userlogin.ProviderKey;
                model.ProfilePic = string.Format("https://graph.facebook.com/{0}/picture", userlogin.ProviderKey);
            }

            return View(model);
        }

        public IActionResult Dashboard()
        {
            return View();
        }
    }
}
