﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Mvc;
using SwoleFile.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using System.Threading.Tasks;
using SwoleFile.Entities;
// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SwoleFile.Controllers
{
    [Route("api/[controller]")]
    public class ProfileController : Controller
    {
        // GET: api/values
        [HttpGet]
        public LoginUserModel Get()
        {
            LoginUserModel model = new LoginUserModel();
            IdentityUserLogin<string> userlogin = new IdentityUserLogin<string>();
            ApplicationUser curUser = GetUserById(User.GetUserId());

            using (var dbContext = new ApplicationDbContext())
            {
                userlogin = dbContext.UserLogins.Where(x => x.UserId == User.GetUserId()).FirstOrDefault();

                if (userlogin != null)
                {
                    model.ProviderKey = userlogin.ProviderKey;
                    model.ProfilePic = string.Format("https://graph.facebook.com/{0}/picture?type=large", userlogin.ProviderKey);
                }
                else
                {
                    model.ProviderKey = String.Empty;
                    model.ProfilePic = @"/images/anon_profile.jpg";
                }

                if (string.IsNullOrEmpty(curUser.CurrentPlanId))
                {
                    model.CurrentPlan = "None";
                    model.CurrentWorkout = "None";
                    model.PlanProgress = "0 / 0";
                    model.CurrentPlanId = -1;
                }
                else
                {
                    var maxStageNum = "0";
                    var wtp = dbContext.WorkoutsToPlans.Where(w => w.PlanId == curUser.CurrentPlanId).OrderByDescending(w => w.StageNumber).FirstOrDefault();
                    if (wtp != null && !string.IsNullOrEmpty(wtp.StageNumber))
                        maxStageNum = wtp.StageNumber;
                    var plan = dbContext.WorkoutPlans.Where(p => p.Id.ToString() == curUser.CurrentPlanId).First();
                    if (plan != null && !string.IsNullOrEmpty(plan.Name))
                    {
                        model.CurrentPlan = plan.Name;
                        model.CurrentPlanId = plan.Id;
                    }
                    var workoutId = dbContext.WorkoutsToPlans.Where(w => w.PlanId == curUser.CurrentPlanId && w.StageNumber == curUser.CurrentStageNumber.ToString()).First().WorkoutId;
                    var workout = dbContext.Workouts.Where(w => w.Id == Convert.ToInt32(workoutId)).First();
                    if (workout != null && !string.IsNullOrEmpty(workout.Name))
                        model.CurrentWorkout = workout.Name;
                    model.PlanProgress = String.Format("{0} / {1}", curUser.CurrentStageNumber.ToString(), maxStageNum);
                }

                model.DisplayName = curUser.DisplayName;

                model.Plans = new List<WorkoutPlan>();
                foreach (var item in dbContext.WorkoutPlans)
                {
                    WorkoutPlan plan = new WorkoutPlan();
                    plan.Id = item.Id;
                    plan.Name = item.Name;
                    model.Plans.Add(plan);
                }

            }

            return model;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public LoginUserModel Get(string id)
        {
            LoginUserModel model = new LoginUserModel();
            IdentityUserLogin<string> userlogin = new IdentityUserLogin<string>();
            ApplicationUser curUser = GetUserById(id);

            using (var dbContext = new ApplicationDbContext())
            {
                userlogin = dbContext.UserLogins.Where(x => x.UserId == id).FirstOrDefault();

                if (userlogin != null)
                {
                    model.ProviderKey = userlogin.ProviderKey;
                    model.ProfilePic = string.Format("https://graph.facebook.com/{0}/picture?type=large", userlogin.ProviderKey);
                }
                else
                {
                    model.ProviderKey = String.Empty;
                    model.ProfilePic = @"/images/anon_profile.jpg";
                }

                if (string.IsNullOrEmpty(curUser.CurrentPlanId))
                {
                    model.CurrentPlan = "None";
                    model.CurrentWorkout = "None";
                    model.CurrentPlanId = dbContext.WorkoutPlans.Where(p => p.Id.ToString() == curUser.CurrentPlanId).First().Id;
                    model.PlanProgress = "0 / 0";
                    model.CurrentPlanId = -1;
                }
                else
                {
                    var maxStageNum = "0";
                    var wtp = dbContext.WorkoutsToPlans.Where(w => w.PlanId == curUser.CurrentPlanId).OrderByDescending(w => w.StageNumber).FirstOrDefault();
                    if (wtp != null && !string.IsNullOrEmpty(wtp.StageNumber))
                        maxStageNum = wtp.StageNumber;
                    var plan = dbContext.WorkoutPlans.Where(p => p.Id.ToString() == curUser.CurrentPlanId).First();
                    if (plan != null && !string.IsNullOrEmpty(plan.Name))
                    {
                        model.CurrentPlan = plan.Name;
                        model.CurrentPlanId = plan.Id;
                    }
                    var workoutId = dbContext.WorkoutsToPlans.Where(w => w.PlanId == curUser.CurrentPlanId && w.StageNumber == curUser.CurrentStageNumber.ToString()).First().WorkoutId;
                    var workout = dbContext.Workouts.Where(w => w.Id == Convert.ToInt32(workoutId)).First();
                    if (workout != null && !string.IsNullOrEmpty(workout.Name))
                        model.CurrentWorkout = workout.Name;
                    model.PlanProgress = String.Format("{0} / {1}", curUser.CurrentStageNumber.ToString(), maxStageNum);
                }

                model.DisplayName = curUser.DisplayName;
            }

            return model;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]LoginUserModel model)
        {
            if (model != null)
            {
                using (var dbContext = new ApplicationDbContext())
                {
                    var user = dbContext.Users.Where(x => x.Id == User.GetUserId()).First();
                    user.DisplayName = model.DisplayName;

                    //Plan changes currently based on CurrentPlan returned in the model
                    var newPlan = dbContext.WorkoutPlans.Where(x => x.Name == model.CurrentPlan).First();
                    if (newPlan.Id != model.CurrentPlanId)
                    {
                        user.CurrentPlanId = newPlan.Id.ToString();
                        user.CurrentStageNumber = 1;
                    }

                    dbContext.SaveChanges();
                }

            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        /// <summary>
        /// Gets current user from database
        /// </summary>
        /// <returns></returns>
        private ApplicationUser GetUserById(string userId)
        {
            ApplicationUser appUser = new ApplicationUser();

            using (var dbContext = new ApplicationDbContext())
            {
                var dbUser = dbContext.Users.Where(x => x.Id == userId).First();
                if (dbUser != null)
                {
                    appUser.AccessFailedCount = dbUser.AccessFailedCount;
                    appUser.ConcurrencyStamp = dbUser.ConcurrencyStamp ?? "";
                    appUser.CurrentPlanId = dbUser.CurrentPlanId ?? "";
                    appUser.CurrentStageNumber = dbUser.CurrentStageNumber;
                    appUser.DisplayName = dbUser.DisplayName ?? "";
                    appUser.Email = dbUser.Email ?? "";
                    appUser.EmailConfirmed = dbUser.EmailConfirmed;
                    appUser.Id = dbUser.Id ?? "";
                    appUser.LockoutEnabled = dbUser.LockoutEnabled;
                    appUser.LockoutEnd = dbUser.LockoutEnd;
                    appUser.NormalizedEmail = dbUser.NormalizedEmail ?? "";
                    appUser.NormalizedUserName = dbUser.NormalizedUserName ?? "";
                    appUser.PasswordHash = dbUser.PasswordHash ?? "";
                    appUser.PhoneNumber = dbUser.PhoneNumber ?? "";
                    appUser.PhoneNumberConfirmed = dbUser.PhoneNumberConfirmed;
                    appUser.SecurityStamp = dbUser.SecurityStamp;
                    appUser.TwoFactorEnabled = dbUser.TwoFactorEnabled;
                    appUser.UserName = dbUser.UserName ?? "";
                }
            }

            return appUser;
        }
    }
}
