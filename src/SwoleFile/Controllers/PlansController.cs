﻿using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Mvc;
using SwoleFile.Models;
using SwoleFile.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;

namespace SwoleFile.Controllers
{
    [Authorize]
    public class PlansController : Controller
    {
        public PlansController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        public SignInManager<ApplicationUser> SignInManager { get; private set; }

        //
        // GET: /Plans/ViewPlans
        //public IActionResult ViewPlans()
        //{
        //    var model = GetAllViewPlansModel();

        //    return View(model);
        //}

        //
        // GET: /Plans/ViewPlans
        public IActionResult ViewPlans(int id)
        {
            var model = GetAllViewPlansModel(id);

            return View(model);
        }

        //
        // GET: /Plans/CurrentWorkout
        [HttpGet]
        public async Task<IActionResult> CurrentWorkout()
        {
            CurrentWorkoutViewModel model = new CurrentWorkoutViewModel { PlanName = "", CurrentStage = "0", Exercises = new List<Exercise>() };

            var user = await GetCurrentUserAsync();
            if (string.IsNullOrEmpty(user.CurrentPlanId))
            {
                ViewBag.Error = "User has not selected a plan.";
                model.PlanName = "None Selected";
                return View(model);
            }
            using (var ctx = new ApplicationDbContext())
            {
                // Check if user plan actually exists
                if (!ctx.WorkoutPlans.Any(p => p.Id.ToString() == user.CurrentPlanId))
                {
                    ViewBag.Error = "Couldn't find user's selected plan.";
                    model.PlanName = "Error";
                    return View(model);
                }

                // Check if user's current workout actually exists
                if (!ctx.WorkoutsToPlans.Any(wtp => (wtp.PlanId == user.CurrentPlanId) && (wtp.StageNumber == user.CurrentStageNumber.ToString())))
                {
                    ViewBag.Error = "Couldn't find user's current workout.";
                    model.PlanName = "Error";
                    return View(model);
                }

                var wtpId = ctx.WorkoutsToPlans.Where(wtp => (wtp.PlanId == user.CurrentPlanId) && (wtp.StageNumber == user.CurrentStageNumber.ToString())).FirstOrDefault();
                var workout = ctx.Workouts.Where(w => w.Id.ToString() == wtpId.WorkoutId).FirstOrDefault();
                var etw = ctx.ExercisesToWorkouts.Where(e => e.WorkoutId == workout.Id.ToString()).ToList();
                List<Exercise> exercises = new List<Exercise>();
                foreach (var entry in etw)
                {
                    var exercise = ctx.Exercises.Where(ex => ex.Id.ToString() == entry.ExerciseId).FirstOrDefault();
                    if (exercise != null)
                        exercises.Add(new Exercise { Id = exercise.Id, Description = exercise.Description, Name = exercise.Name });
                }

                model.PlanName = ctx.WorkoutPlans.Where(p => p.Id.ToString() == user.CurrentPlanId).FirstOrDefault().Name;
                model.WorkoutName = workout.Name;
                model.CurrentStage = user.CurrentStageNumber.ToString();
                model.Exercises = exercises;
            }

            return View(model);
        }

        //
        // POST: /Manage/CompleteWorkout
        [HttpPost]
        public async Task<IActionResult> CompleteWorkout()
        {
            var user = await GetCurrentUserAsync();

            using (var ctx = new ApplicationDbContext())
            {
                // See if there is another workout in the plan
                if (ctx.WorkoutsToPlans.Any(wtp => wtp.PlanId == user.CurrentPlanId && wtp.StageNumber == (user.CurrentStageNumber + 1).ToString()))
                {
                    user.CurrentStageNumber++;
                }
                else
                {
                    // Finished with plan, revert to first stage
                    user.CurrentStageNumber = 1;
                }
            }

            await UserManager.UpdateAsync(user);

            return RedirectToAction("Profile", "Home");
        }

        #region Helpers
        private async Task<ApplicationUser> GetCurrentUserAsync()
        {
            return await UserManager.FindByIdAsync(Context.User.GetUserId());
        }

        private List<ViewPlansViewModel> GetAllViewPlansModel(int selected = -1)
        {
            List<ViewPlansViewModel> model = new List<ViewPlansViewModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                // Create a view model for each plan
                foreach (var plan in dbContext.WorkoutPlans.ToList())
                {
                    ViewPlansViewModel p = new ViewPlansViewModel { PlanName = plan.Name, Workouts = new List<Workout>(), IsSelected = false };

                    // Get all workouts for this plan
                    var workouts = dbContext.WorkoutsToPlans.Where(wtp => wtp.PlanId == plan.Id.ToString()).ToList();
                    foreach (var wtp in workouts)
                    {
                        // Find this workout
                        var workout = dbContext.Workouts.Where(w => w.Id.ToString() == wtp.WorkoutId).FirstOrDefault();

                        // Instantiate a workout
                        var wo = new Workout { Id = workout.Id, Description = workout.Description, Name = workout.Name, Exercises = new List<Exercise>() };

                        // Get all exercises for this workout
                        var exercises = dbContext.ExercisesToWorkouts.Where(etw => etw.WorkoutId == workout.Id.ToString()).ToList();
                        foreach (var etw in exercises)
                        {
                            // Add this exercise to this workout
                            var exercise = dbContext.Exercises.Where(e => e.Id.ToString() == etw.ExerciseId).FirstOrDefault();
                            wo.Exercises.Add(new Exercise { Name = exercise.Name, Id = exercise.Id, Description = exercise.Description });
                        }

                        // Add this workout to the plan
                        p.Workouts.Add(wo);
                    }

                    // Check if this is the selected plan
                    if (selected != -1 && selected == plan.Id)
                        p.IsSelected = true;

                    // Add this plan to the list of view models
                    model.Add(p);
                }
            }

            return model;
        }
        #endregion
    }
}
