﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using SwoleFile.Models;
using SwoleFile.Entities;
using System.Security.Claims;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SwoleFile.Controllers
{
    [Route("api/[controller]")]
    public class Dashboard : Controller
    {
        public IActionResult Index()
        {
            List<CheckInIndexModel> model = new List<CheckInIndexModel>();
            //using (var ctx = new ApplicationDbContext())
            //{

            //    var users = ctx.Users.ToList();
            //    foreach (var u in users)
            //    {
            //        var cnt = ctx.CheckIns.Where(p => p.UserId == u.Id).Count();
            //        var displayName = string.IsNullOrEmpty(u.DisplayName) ? u.Email : u.DisplayName;
            //        var planName = "None";
            //        if (ctx.WorkoutPlans.Any(p => p.Id.ToString() == u.CurrentPlanId))
            //        {
            //            planName = ctx.WorkoutPlans.Where(p => p.Id.ToString() == u.CurrentPlanId).First().Name;
            //        }
            //        model.Add(new CheckInIndexModel() { Email = u.Email, CheckinCount = cnt, DisplayName = displayName, CrntPlan = planName });
            //    }
            //    model = model.OrderByDescending(p => p.CheckinCount).ToList();
            //}
            return View(model);
        }

        [HttpGet]
        public CheckInIndexModel GetAll()
        {

            CheckInIndexModel model = new CheckInIndexModel();            
            List<CheckInModel> list = new List<CheckInModel>();
            model.CheckIns = list;
            model.AllowCheckIn = true;
            using (var ctx = new ApplicationDbContext())
            {

                var users = ctx.Users.ToList();
                foreach (var u in users)
                {
                    var cnt = ctx.CheckIns.Where(p => p.UserId == u.Id).Count();
                    var displayName = string.IsNullOrEmpty(u.DisplayName) ? u.Email : u.DisplayName;
                    var planName = "None";
                    if (ctx.WorkoutPlans.Any(p => p.Id.ToString() == u.CurrentPlanId))
                    {
                        planName = ctx.WorkoutPlans.Where(p => p.Id.ToString() == u.CurrentPlanId).First().Name;
                    }
                    list.Add(new CheckInModel() { Email = u.Email, CheckinCount = cnt, DisplayName = displayName, CrntPlan = planName });
                }

                model.CheckIns = list.OrderByDescending(p => p.CheckinCount).ToList();

                if (User.Identity.IsAuthenticated)
                {
                    var cnt = ctx.CheckIns.Where(p1 => p1.UserId == User.GetUserId()).Select(p2=>p2).OrderByDescending(p3=>p3.TimeStamp).FirstOrDefault();
                    if(cnt!=null)
                    {
                        model.AllowCheckIn = cnt.TimeStamp.AddHours(1) < DateTime.Now;
                    }
                }
            }

            return model;
        }

    }
}
