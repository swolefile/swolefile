﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Framework.OptionsModel;
using SwoleFile.Entities;
using System.ComponentModel;

namespace SwoleFile.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string CurrentPlanId { get; set; }

        public int CurrentStageNumber { get; set; }

        public string DisplayName { get; set; }

        public ApplicationUser()
        {
            CurrentStageNumber = 1;
        }
                
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        private static bool _created;

        #region Tables
        public DbSet<CheckIn> CheckIns { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<ExercisesToWorkouts> ExercisesToWorkouts { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<WorkoutPlan> WorkoutPlans { get; set; }
        public DbSet<WorkoutsToPlans> WorkoutsToPlans { get; set; }
        #endregion

        public ApplicationDbContext()
        {
            // Create the database and schema if it doesn't exist
            if (!_created)
            {
                Database.AsRelational().ApplyMigrations();
                _created = true;
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Ignore these columns
            builder.Entity<Workout>().Ignore(p => p.Exercises);
            builder.Entity<WorkoutPlan>().Ignore(p => p.Workouts);

            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);            

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var conn = @"Server = tcp:swoledb.database.windows.net,1433; Database = swoledb; User ID = swolefile@swoledb; Password = ShareFile123; Trusted_Connection = False; Encrypt = True; Connection Timeout = 30;";
            optionsBuilder.UseSqlServer(conn);
        }
    }
}
