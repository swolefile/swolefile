﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SwoleFile.Models
{
    public class CheckInIndexModel
    {
        public List<CheckInModel> CheckIns { get; set; }
        public bool AllowCheckIn { get; set; }
    }

    public class CheckInModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public int CheckinCount { get; set; }
        public string CrntPlan { get; set; }
        public string userId { get; set; }
        public string DisplayName { get; set; }
    }
}
