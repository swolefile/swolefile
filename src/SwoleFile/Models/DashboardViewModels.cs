﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SwoleFile.Models
{     
    public class DashboardViewModel
    {
        public string DisplayName { get; set; }

        public string CurrentPlan { get; set; }

        public int CheckIns { get; set; }
    }

}
