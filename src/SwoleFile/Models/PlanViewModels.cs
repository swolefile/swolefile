﻿using SwoleFile.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SwoleFile.Models
{
    public class CurrentWorkoutViewModel
    {
        public string PlanName;
        public string CurrentStage;
        public string WorkoutName;
        public List<Exercise> Exercises;
    }

    public class ViewPlansViewModel
    {
        public string PlanName;
        public bool IsSelected;
        public List<Workout> Workouts;
    }
}
